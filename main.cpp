#include <iostream>
#include <locale.h>

const char* season(unsigned hours) {
	if (0 <= hours && hours <= 4)
		return "ночи";
	else if (5 <= hours && hours <= 11)
		return "утра";
	else if (12 <= hours && hours <= 17)
		return "дня";
	else if (18 <= hours && hours <= 23)
		return "вечера";
	
	return "unexpected error";
}

const char* text_h(unsigned hours) {
	if (hours == 1)
		return "час";
	else if (2 <= hours && hours <= 4)
		return "часа";
	
	return "часов";
}

const char* text_m(unsigned minutes) {
	if (11 <= minutes && minutes <= 14)
		return "минут";
	if (minutes%10 == 1)
		return "минута";
	if (2 <= minutes%10 && minutes%10 <= 4)
		return "минуты";
	
	return "минут";
}

unsigned limit_h(unsigned hours) {
	if (hours%12 == 0)
		return 12;
	
	return hours % 12;
}

int main(int argc, char** argv)
{
	// support russian symbols
	setlocale(LC_ALL,"");

	// define variables
	unsigned hours = 0, minutes = 0;
	
	// handle standard input
	std::cin >> hours >> minutes;

	// check for input correctness
	if (!(0 <= hours && hours <= 23 && 0 <= minutes && minutes <= 59)) {
		std::cout << "Неверные входные данные" << std::endl;
		return 1;
	}

	// special time
	if (hours == 12 && minutes == 0) {
		std::cout << "Полдень" << std::endl;
		return 0;
	}
	if (hours == 0 && minutes == 0) {
		std::cout << "Полночь" << std::endl;
		return 0;
	}
	
	// output translated time
	std::cout << limit_h(hours) << " " << text_h(limit_h(hours)) << " ";
	if (minutes != 0)
		std::cout << minutes << " " << text_m(minutes) << " ";
	std::cout << season(hours);
	if (minutes == 0)
		std::cout << " ровно";
	std::cout << std::endl;

	return 0;
}