#!/bin/sh

function clocks()
{
  echo " input: $1 $2"
  echo -n "output: "
  ./clocks <<< "$1 $2"
  echo "  exit: $?"
  echo
}

## special time ("полночь", "полдень")
clocks 00 00
clocks 12 00

## wrong time ("Неверные входные данные")
clocks 24 00

## exact time ("часов", "утра", "ровно")
clocks 05 00

## ("час", "минута", "дня")
clocks 13 01

## time in 12-hours format ("часов", "минут", "вечера")
clocks 21 11

## ("часа", "минуты", "ночи")
clocks 02 02